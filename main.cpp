#include "aux.h"

#include <sys/swap.h>
#include <sys/vfs.h>

#include <cstdint>
#include <filesystem>
#include <string>
#include <string_view>
#include <thread>

constexpr auto ramSize{ std::uintmax_t{3}*1024*1024*1024 };

void mountVRAM(std::string_view vramDir) {
	std::filesystem::create_directories(vramDir);

	auto sizeArg{ std::to_string(ramSize) };
	run("vramfs", vramDir.data(), sizeArg.c_str());
}

void waitForMount(std::string_view path, int f_type) {
	struct statfs buf{};
	while (true)
	{
		statfs(path.data(), &buf);
		if (buf.f_type == f_type) break;
	}
}

void setupSwap(const std::string& path) {
	createFile(path);

	std::thread resizeSwapThread{
		static_cast<
			void(*)(const std::filesystem::path&, std::uintmax_t)
		>(std::filesystem::resize_file),
		path,
		ramSize
	};

	const auto loopFileName{ "/dev/loop" + std::to_string(getLoopNo()) };

	resizeSwapThread.join();

	assocLoop(loopFileName, path);

	run("mkswap", loopFileName.data());
	
	swapon(loopFileName.data(), SWAP_FLAG_PREFER);
}

int main() {
	using namespace std::literals;
	constexpr auto vramDir{ "/run/media/vram/"sv };

	std::jthread vramThread{ mountVRAM, vramDir };

	waitForMount(vramDir, 0x65735546); // FUSE (statfs(2))

	setupSwap(std::string{ vramDir } + "/swapfile");
	
	if (fork() != 0) _exit(0);
}
