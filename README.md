setupvramswap
=============

This simple program mounts a 3 GiB VRAM file system at /run/media/vram/ and
creates a swapfile (called "swapfile") in that directory. A systemd service has
been included.

Dependencies
------------

- [vramfs](https://github.com/Overv/vramfs)

Building and installation
-------------------------

```sh
git clone https://gitlab.com/glibg10b/setupvramswap
cd setupvramswap
sudo c++ main.cpp -std=c++23 -I. -Iaux -o /usr/local/bin/setupvramswap
sudo install setupvramswap.service /etc/systemd/system/
sudo systemctl enable setupvramswap
sudo systemctl start setupvramswap
```
