#include <fcntl.h>
#include <string_view>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>

class POSIXFile {
public:
	POSIXFile(std::string_view path, int oflag)
		: m_fd{ open(path.data(), oflag) }
	{}

	~POSIXFile() {
		close(m_fd);
	}

	auto fd() { return m_fd; }
private:
	const int m_fd{};
};
