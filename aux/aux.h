#include "POSIXFile.h"

#include <fcntl.h>
#include <linux/loop.h>
#include <string>
#include <sys/ioctl.h>
#include <sys/wait.h>
#include <unistd.h>

#include <fstream>

template <typename... Args>
void run(auto prog, const Args&... args) {
	//int fds[2]{};
	//if (pipe(fds)){};
	const auto pid{ fork() };
	if (pid == 0)
	{
		//dup2(fds[0], 0);
		//dup2(fds[1], 1);
		//close(fds[0]);
		//close(fds[1]);
		execlp(prog, prog, args..., nullptr);
	}

	//close(fds[0]);
	//close(fds[1]);

	waitpid(pid, nullptr, 0);
}

inline void createFile(const std::string& path) {
	std::ofstream{ path };
}

inline auto getLoopNo() {
	const auto loopctlfd{ open("/dev/loop-control", O_RDWR) };
	
	const auto loopNo{ ioctl(loopctlfd, LOOP_CTL_GET_FREE) };
	
	close(loopctlfd);

	return loopNo;
}

inline void assocLoop(std::string_view loopName, std::string_view path) {
	POSIXFile loop{ loopName, O_RDWR };
	POSIXFile file{ path, O_RDWR };

	ioctl(loop.fd(), LOOP_SET_FD, file.fd());
}
